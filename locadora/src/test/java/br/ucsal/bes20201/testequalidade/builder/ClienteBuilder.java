package br.ucsal.bes20201.testequalidade.builder;

import br.ucsal.bes20201.testequalidade.locadora.dominio.Cliente;

public class ClienteBuilder {
	
	private static final String CPF_DEFAULT = ("00000000000");
	
	private static final String NOME_DEFAULT = ("Tiago");
	
	private static final String TELEFONE_DEFAULT = ("99999-9999");
	
	
	private String cpf = CPF_DEFAULT;
	
	private String nome = NOME_DEFAULT;
	
	private String telefone = TELEFONE_DEFAULT;
	
	public ClienteBuilder() {
		
	}
	
	public static ClienteBuilder umCliente() {
	return new ClienteBuilder();	
	}
	
	public ClienteBuilder comCPF(String cpf) {
		this.cpf = cpf;
		return this;
	}
	public ClienteBuilder comNome(String nome) {
		this.nome = nome;
		return this;
	}
	public ClienteBuilder comTelefone(String telefone) {
		this.telefone = telefone;
		return this;
	}
	
	public ClienteBuilder mas() {
		return umCliente().comCPF(cpf).comNome(nome).comTelefone(telefone);
		
	}

	public Cliente build() {
		Cliente cliente = new Cliente(cpf, nome, telefone);
		cliente.setCpf(cpf);
		cliente.setNome(nome);
		cliente.setTelefone(telefone);
		return cliente;
	}

}
