package br.ucsal.bes20201.testequalidade.builder;

import br.ucsal.bes20201.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;



public class VeiculoBuilder {
	
	//static ModeloBuilder modeloDefault = new ModeloBuilder("veiculo1");
	

	private static final String PLACA_DEFAULT = "QQQ-0000";

	private static final Integer ANO_DEFAULT = 1;

	private static final Modelo MODELO_DEFAULT = new Modelo("GOL");

	private static final Double VALORDIARIA_DEFAULT = 50d;

	private static final SituacaoVeiculoEnum SITUACAO_DEFAULT = SituacaoVeiculoEnum.DISPONIVEL;
	
	private String placa = PLACA_DEFAULT;

	private Integer ano = ANO_DEFAULT;

	private  Modelo modelo = MODELO_DEFAULT;

	private Double valorDiaria = VALORDIARIA_DEFAULT;

	private SituacaoVeiculoEnum situacao = SITUACAO_DEFAULT;



	public static VeiculoBuilder umVeiculo() {
		return new VeiculoBuilder();
	}
	
	public VeiculoBuilder comPlaca(String placa) {
		this.placa = placa;
		return this;
	}
	public VeiculoBuilder comAno(Integer ano) {
		this.ano = ano;
		return this;
	}
	public VeiculoBuilder comModelo(Modelo modelo) {
		this.modelo = modelo;
		return this;
	}
	public VeiculoBuilder comValorDiaria(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
		return this;
	}
	public VeiculoBuilder comSituacao(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao;
		return this;
	}
	
	public VeiculoBuilder mas() {
		return umVeiculo()
				.comAno(ano)
				.comModelo(modelo)
				.comPlaca(placa)
				.comSituacao(situacao)
				.comValorDiaria(valorDiaria);
				
	}
	public Veiculo build() {
		Veiculo veiculo = new Veiculo();
		veiculo.setAno(ano);
		veiculo.setModelo(modelo);
		veiculo.setPlaca(placa);
		veiculo.setSituacao(situacao);
		veiculo.setValorDiaria(valorDiaria);
		
		return veiculo;
		
	}
}
