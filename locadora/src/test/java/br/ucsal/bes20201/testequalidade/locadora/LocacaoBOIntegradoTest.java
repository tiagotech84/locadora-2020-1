package br.ucsal.bes20201.testequalidade.locadora;

import java.util.Date;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import br.ucsal.bes20201.testequalidade.builder.ClienteBuilder;
import br.ucsal.bes20201.testequalidade.builder.LocacaoBuilder;
import br.ucsal.bes20201.testequalidade.builder.VeiculoBuilder;
import br.ucsal.bes20201.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Cliente;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Locacao;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.util.ValidadorUtil;

public class LocacaoBOIntegradoTest {

	/**
	 * Testar o cÃ¡lculo do valor de locaÃ§Ã£o por 3 dias para 2 veÃ­culos com 1 ano de
	 * fabricaÃ§ao e 3 veÃ­culos com 8 anos de fabricaÃ§Ã£o.
	 */
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() {
		
	}
	@Test
	public static void main(String[] args) {
		
		
		Integer QtdDias = 3;
		@SuppressWarnings("deprecation")
		//Date data = new Date(2020, 3, 8);
		
		Date data2 = new Date();
		
		List<Veiculo> veiculos = new ArrayList<>();
		List<Cliente> clientes = new ArrayList<>();
		
		
		Cliente cliente1 = new ClienteBuilder().build();
		Veiculo veiculo1 = new VeiculoBuilder().comAno(1).build();
		Veiculo veiculo2 = new VeiculoBuilder().comAno(2).build();
		Veiculo veiculo3 = new VeiculoBuilder().comPlaca("JJJ-9999").build();
		Veiculo veiculo4 = new VeiculoBuilder().comAno(9).build();
		Veiculo veiculo5 = new VeiculoBuilder().comAno(10).build();
		
		veiculos.add(veiculo1);
		veiculos.add(veiculo2);
		veiculos.add(veiculo3);
		veiculos.add(veiculo4);
		veiculos.add(veiculo5);
	
		Locacao locacao = new LocacaoBuilder().build();
		locacao.getVeiculos();
		clientes.add(cliente1);
		
		
		
		Locacao locacao1 = new Locacao(cliente1, veiculos, data2, QtdDias);
		
		
		LocacaoBO locacaoBO = new LocacaoBO();	
		
		//System.out.println(veiculo1.getPlaca() + "  veiculos");
		//System.out.println(cliente1.getNome() + "  clientes");
		
		
		locacaoBO.calcularValorTotalLocacao(veiculos, QtdDias);
	}

}
