package br.ucsal.bes20201.testequalidade.builder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.ucsal.bes20201.testequalidade.locadora.dominio.Cliente;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Locacao;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;

public class LocacaoBuilder {
	
	private static final Integer SEQ_DEFAULT = 0;
	
	private static final Integer NUMERO_CONTRATO_DEFAULT = 1;

	private static final Cliente CLIENTE_DEFAULT = new ClienteBuilder().build();

	private static final List<Veiculo> VEICULO_DEFAULT = new ArrayList <>();

	private static final Date DATA_LOCACAO_DEFAULT = new Date(2020, 3, 8);

	private static final Integer QUANTIDADES_DIAS_LOCACAO_DEFAULT = 3;

	private static final Date DATA_DEVOLUCAO_DEFAULT = new Date(2020, 3, 11);
	
	
	private static Integer seq = SEQ_DEFAULT;
	
	private Integer numeroContrato = NUMERO_CONTRATO_DEFAULT;

	private Cliente cliente = CLIENTE_DEFAULT;

	private List<Veiculo> veiculos =VEICULO_DEFAULT;

	private Date dataLocacao = DATA_LOCACAO_DEFAULT;

	private Integer quantidadeDiasLocacao = QUANTIDADES_DIAS_LOCACAO_DEFAULT;

	private Date dataDevolucao = DATA_DEVOLUCAO_DEFAULT;
	
	
	
	public LocacaoBuilder umaLocacao() {
		return new LocacaoBuilder();
	}
	
	public LocacaoBuilder comCliente(Cliente cliente) {
		this.cliente = cliente;
		return this;
	}
	
	public LocacaoBuilder comVeiculos(List<Veiculo> veiculos) {
		this.veiculos = veiculos;
		return this;
	}
	public LocacaoBuilder comDataLocacao(Date dataLocacao) {
		this.dataLocacao = dataLocacao;
		return this;
	}
	public LocacaoBuilder comQuantidadeDiasLocacao(Integer quantidadeDiasLocacao) {
		this.quantidadeDiasLocacao = quantidadeDiasLocacao;
		return this;
	}
	public LocacaoBuilder comDataDevolucao(Date dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
		return this;
	}
	public LocacaoBuilder comNumeroContrato(Integer numeroContrato) {
		this.numeroContrato = numeroContrato;
		return this;
	}
	public LocacaoBuilder mas(Integer numeroContrato) {
		return umaLocacao().comCliente(cliente).comDataDevolucao(dataDevolucao)
		.comDataLocacao(dataLocacao).comNumeroContrato(numeroContrato)
		.comQuantidadeDiasLocacao(quantidadeDiasLocacao).comVeiculos(veiculos);
		
	}
	
	public Locacao build() {
		Locacao locacao = new Locacao(cliente, veiculos, dataDevolucao, numeroContrato);
		return locacao;
	}

}
